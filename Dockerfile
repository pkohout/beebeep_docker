FROM ubuntu
RUN apt-get update && apt-get install -y libqt5core5a libqt5dbus5 libqt5gui5 libqt5multimedia5  libqt5network5 libqt5printsupport5 libqt5svg5 libqt5widgets5 libqt5x11extras5 libxcb-screensaver0 wget
COPY ./beebeep_5.6.8-1_amd64.deb beebeep_5.6.8-1_amd64.deb 
RUN dpkg -i /beebeep_5.6.8-1_amd64.deb
CMD beebeep
