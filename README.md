# BeeBeep_Docker

Just a docker container that is able to run beebeep messenger. deb file downloaded from: https://www.beebeep.net/
Running:
```
docker run -it -e DISPLAY=$DISPLAY --network=host --privileged  registry.gitlab.com/pkohout/beebeep_docker
```